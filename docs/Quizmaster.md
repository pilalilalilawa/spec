# Quizmaster

## State Diagram

## Messages

### RegisterQuizmaster

```ts
interface RegisterQuizmaster {
    type: "RegisterQuizmaster"
    quiz: Quiz // See Quiz.md
}
```

### Go

```ts
interface Go {
    type: "Go"
}
```

### Next

```ts
interface Next {
    type: "Next"
}
```
