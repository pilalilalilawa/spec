# Server

## State Diagram

```
       +---------+
       | Initial | Receive Register from clients
       +---------+
            |
            | Receive Go
            | Send AskQuestion to all clients and the presenter
            V
    +---------------+
+-->| QuestionAsked | Receive GiveAnswer from clients
|   +---------------+
|           |
|           | All clients answered or timeout
|           | Send Feedback to all clients, send report results to presenter,
|           | notify quizmaster
|           V
|       +-------+  No questions remaining  +----------+
|       | Pause |------------------------->| Finished |
|       +-------+                          +----------+
|           |
|           | Questions remaining
|           | Recive Next
+-----------+
```

## Messages

### To Participants and Presenter

#### AskQuestion

```ts
interface AskQuestion {
    tag: "AskQuestion"
    id: number
    question: string
    answers: Answers
}

interface Answers {
    a: string
    b: string
    c: string
    d: string
}
```

### To Participants

#### WelcomeParticipant

```ts
interface WelcomeParticipant {
    tag: "WelcomeParticipant"
}
```

#### Kicked

```ts
interface Kicked {
    tag: "Kicked"
}
```

#### GiveFeedback

```ts
interface GiveFeedback {
    tag: "GiveFeedback"
    correct: boolean
    moreQuestions: boolean
}
```

### To Presenter

#### WelcomePresenter

```ts
interface WelcomePresenter {
    tag: "WelcomePresenter"
}
```

#### ReportResults

```ts
interface ReportResults {
    tag: "ReportResults"
    answers: ReportAnswers
    ranking: RankingEntry[]
    moreQuestions: boolean
}

interface ReportAnswers {
    a: ReportAnswer
    b: ReportAnswer
    c: ReportAnswer
    d: ReportAnswer
}

interface ReportAnswer {
    correct: boolean
    guesses: number
}

interface RankingEntry {
    nickname: string
    score: number
}
```

### To Quizmaster

#### WelcomeQuizmaster

```ts
interface WelcomeQuizmaster {
    tag: "WelcomeQuizmaster"
    key: string
}
```

#### ReportAnswered

```ts
interface ReportAnswered {
    tag: "ReportAnswered"
}
```
