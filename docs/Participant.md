# Participant

## State Diagram

```
        +------+
        | Init |
        +------+
            |
            | Send Register
            V
    +---------------------+
    | PendingRegistration |
    +---------------------+
            |
            | Receive Welcome
            V
        +-------+
        | Ready |
        +-------+
            |
            | Receive AskQuestion
            V
     +---------------+
+--->| QuestionAsked |
|    +---------------+
|           |
|           | Send GiveAnswer
|           V
|     +-------------+
|     | AnswerGiven |
|     +-------------+
|           |
|           | Registration
|           V
|   +------------------+  No more questions   +----------+
|   | FeedbackReceived |--------------------->| Finished |
|   +------------------+                      +----------+
|           |
|           | More questions
|           | ReceiveAskQuestion
+-----------+
```

## Messages

### RegisterParticipant

```ts
interface RegisterParticipant {
    tag: "RegisterParticipant"
    nickname: string
}
```

### GiveAnswer

```ts
interface GiveAnswer {
    tag: "GiveAnswer"
    questionId: number
    answer: "A" | "B" | "C" | "D"
}
```
