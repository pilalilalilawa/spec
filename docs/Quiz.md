# Quiz Definitions

```ts
type Quiz = Round[]

interface Round {
    question: string
    answers: Answers
}

interface Answers {
    a: Answer
    b: Answer
    c: Answer
    d: Answer
}

interface Answer {
    text: string
    correct: boolean
}
```
